# Hello everyone ! #
### This project is modified snippets.json Emmet. ###

* **con**: container

```
#!html

<div class="container"></div>
```

* **bad**: badge

```
#!html

<span class="badge"></span>
```

* **bread**: breadcrumb

```
#!html

        <ol class="breadcrumb">
          <li><a href="#"></a></li>
          <li><a href="#"></a></li>
          <li class="active"></li>
        </ol>
```
* **navi**: navbar navbar-inverse

```
#!html

<nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header"><button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9" tupe="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button><a href="#" class="navbar-brand">Brand</a></div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Link</a></li>
                <li><a href="#"></a></li>
              </ul>
            </div>
          </div>
        </nav>
```